# local git in discord

## Первоначальная настройка
В файле Settings.json указать токен бота

## Синтаксис
Все комманды начинаются с '-'   
Например, -create-branch "branch name"  
Если аргумент команды содержит проблеы, то необходимо аргумент взять в двойные кавычки, если не содержит, то можно и без них

## Доступные команды
* help - вывод краткой информации по командам
* create-branch - создание ветки (-create-branch "branch name")
* delete-branch - удаление ветки (-delete-branch "branch name")
* rename-branch - переименование векти (-rename-branch "old branch name" "new branch name") **ВРЕМЕННО НЕ РАБОТАЕТ**
* commit - коммит на ветку (-commit "branch name" "description" должен содержать файлы для коммита)
