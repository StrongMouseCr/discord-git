﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Newtonsoft.Json;
using CommandInfo = DiscordBot_git.Models.HelpInfo.CommandInfo;

namespace DiscordBot_git.Modules
{
    public class HelpCommands : ModuleBase<SocketCommandContext>
    {
        [Command("help")]
        public async Task GetHelpInfoAsync(string command = "")
        {
            Console.WriteLine(new LogMessage(LogSeverity.Info, "Help", "help command used"));
            var info = JsonConvert.DeserializeObject<List<CommandInfo>>(await File.ReadAllTextAsync("HelpInfo.json"));
            var result = string.Empty;
            if (info != null)
            {
                if (command != string.Empty)
                {
                    var commandInfo = info.FirstOrDefault(c => c.Name == command.ToLower().Trim());
                    if (commandInfo != null)
                        await Context.Channel.SendMessageAsync(commandInfo.ToString());
                    else
                        await Context.Channel.SendMessageAsync($"Команды {command} не найдено");
                }
                else
                {
                    foreach (var commandInfo in info)
                    {
                        result += commandInfo.ToString();
                        result += "----------------\n";
                    }
                    await Context.Channel.SendMessageAsync(result);
                }
            }
        }
    }
}