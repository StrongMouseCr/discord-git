﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace DiscordBot_git.Modules
{
    public class BranchesCommands : ModuleBase<SocketCommandContext>
    {
        [Command("create-branch")]
        public async Task CreateBranchAsync(string repository, string name)
        {
            name = name.ToLower().Trim();
            repository = repository.ToLower().Trim();
            Console.WriteLine(new LogMessage(LogSeverity.Info, "Branches", "create-branch command used"));
            if (Context.Guild.CategoryChannels.FirstOrDefault(c => c.Name.Contains(repository)) != null)
            {
                var id = Context.Guild.CategoryChannels.FirstOrDefault(c => c.Name.Contains(repository))!.Id;
                if (Context.Guild.TextChannels.FirstOrDefault(c => c.Name.Contains(name)) == null)
                {
                    await Context.Guild.CreateTextChannelAsync(name, properties =>
                    {
                        properties.CategoryId = id;
                        properties.PermissionOverwrites = new List<Overwrite>
                        {
                            new(Context.Guild.EveryoneRole.Id, PermissionTarget.Role,
                                new OverwritePermissions(PermValue.Deny, PermValue.Deny, 
                                    PermValue.Deny, PermValue.Inherit, PermValue.Deny, 
                                    PermValue.Deny, PermValue.Deny, PermValue.Deny, 
                                    PermValue.Deny, PermValue.Inherit, PermValue.Deny, 
                                    PermValue.Deny, PermValue.Deny, PermValue.Deny, 
                                    PermValue.Deny, PermValue.Deny, PermValue.Deny, 
                                    PermValue.Deny, PermValue.Deny, PermValue.Deny, 
                                    PermValue.Deny, PermValue.Deny, PermValue.Deny, 
                                    PermValue.Deny, PermValue.Deny, PermValue.Deny, 
                                    PermValue.Deny, PermValue.Deny, PermValue.Deny, 
                                    PermValue.Deny, PermValue.Deny, PermValue.Deny, 
                                    PermValue.Deny)),
                        };
                    });
                    await Context.Channel.SendMessageAsync($"Создана ветка {repository}/{name}");
                }
                else
                {
                    await Context.Channel.SendMessageAsync($"Ветка {repository}/{name} уже существует");
                }
            }
            else
            {
                await Context.Channel.SendMessageAsync(
                    $"Ветка {repository}/{name} не создана, так как нет репозитория {repository}. " +
                    $"Команда для создания репозитория \"-create-rep repository_name\"");
            }
        }
        
        [Command("delete-branch")]
        public async Task DeleteBranchAsync(string name)
        {
            name = name.ToLower().Trim();
            Console.WriteLine(new LogMessage(LogSeverity.Info, "Branches", "delete-branch command used"));
            if (Context.Guild.TextChannels.FirstOrDefault(c => c.Name.Contains(name)) != null)
            {
                await Context.Guild.TextChannels.First(c => c.Name.Contains(name))!.DeleteAsync();
                await Context.Channel.SendMessageAsync($"Ветка git-local/{name} удалена");
            }
            else
            {
                await Context.Channel.SendMessageAsync($"Ветка git-local/{name} не найдена");
            }
        }
        
        //Not work
        // [Command("rename-branch")]
        // public async Task RenameBranchAsync(string oldName, string name)
        // {
        //     Console.WriteLine(new LogMessage(LogSeverity.Info, "Branches", "rename-branch command used"));
        //     if (Context.Guild.TextChannels.FirstOrDefault(c => c.Name.Contains(oldName)) != null)
        //     {
        //         await Context.Guild.TextChannels.First(c => c.Name.Contains(oldName))!
        //             .ModifyAsync(properties => properties.Name = name);
        //         await Context.Channel.SendMessageAsync($"git-local/{oldName} -> git-local/{name}");
        //     }
        //     else
        //     {
        //         await Context.Channel.SendMessageAsync($"Ветка git-local/{name} не найдена");
        //     }
        // }
    }
}