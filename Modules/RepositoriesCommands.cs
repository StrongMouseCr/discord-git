﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Newtonsoft.Json;

namespace DiscordBot_git.Modules
{
    public class RepositoriesCommands : ModuleBase<SocketCommandContext>
    {
        [Command("create-rep")]
        public async Task CreateRepository(string name)
        {
            name = name.ToLower().Trim();
            Console.WriteLine(new LogMessage(LogSeverity.Info, "Repositories", "create-rep command used"));
            if (Context.Guild.CategoryChannels.FirstOrDefault(c => c.Name.Contains(name.ToLower().Trim())) == null)
            {
                await Context.Guild.CreateCategoryChannelAsync(name, properties =>
                {
                    properties.PermissionOverwrites = new List<Overwrite>
                    {
                        new(Context.Guild.EveryoneRole.Id, PermissionTarget.Role,
                            new OverwritePermissions(PermValue.Deny, PermValue.Deny,
                                PermValue.Deny, PermValue.Inherit, PermValue.Deny,
                                PermValue.Deny, PermValue.Deny, PermValue.Deny,
                                PermValue.Deny, PermValue.Deny, PermValue.Deny,
                                PermValue.Deny, PermValue.Deny, PermValue.Deny,
                                PermValue.Deny, PermValue.Deny, PermValue.Deny,
                                PermValue.Deny, PermValue.Deny, PermValue.Deny,
                                PermValue.Deny, PermValue.Deny, PermValue.Deny,
                                PermValue.Deny, PermValue.Deny, PermValue.Deny,
                                PermValue.Deny, PermValue.Deny, PermValue.Deny,
                                PermValue.Deny, PermValue.Deny, PermValue.Deny,
                                PermValue.Deny)),
                    };
                });
                await Context.Channel.SendMessageAsync($"Создан репозиторий {name}");
            }
            else
            {
                await Context.Channel.SendMessageAsync(
                    $"Репозиторий {name} не создан, так как репозиторий с таким именем уже существует");
            }
        }
        
        [Command("delete-rep")]
        public async Task DeleteRepository(string name)
        {
            name = name.ToLower().Trim();
            Console.WriteLine(new LogMessage(LogSeverity.Info, "Repositories", "delete-rep command used"));
            if (Context.Guild.CategoryChannels.FirstOrDefault(c => c.Name.Contains(name)) != null)
            {
                var categoryChannel = Context.Guild.CategoryChannels.FirstOrDefault(c => c.Name.Contains(name));
                foreach (var chanel in categoryChannel!.Channels)
                    await chanel.DeleteAsync();
                await categoryChannel!.DeleteAsync();
                await Context.Channel.SendMessageAsync($"Репозиторий {name} удален");
            }
            else
            {
                await Context.Channel.SendMessageAsync($"Репозиторий {name} не найден");
            }
        }
        
    }
}