﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace DiscordBot_git.Modules
{
    public class CommitCommands : ModuleBase<SocketCommandContext>
    {
        [Command("commit")]
        public async Task CreateBranchAsync(string branchName, string description)
        {
            Console.WriteLine(new LogMessage(LogSeverity.Info, "Commit", "commit command used"));
            var filesNames = new List<string>();
            foreach (var attachment in Context.Message.Attachments)
            {
                var fileName = attachment.Filename;
                var fileUrl = attachment.Url;
                using var webClient = new WebClient();
                webClient.DownloadFile(fileUrl, $"TempFiles/{fileName}");
                filesNames.Add(fileName);
            }

            var filesToSend = filesNames.Select(name => new FileAttachment($"TempFiles/{name}", name)).ToList();

            if (Context.Guild.TextChannels.FirstOrDefault(c => c.Name.Contains(branchName)) != null)
            {
                await Context.Guild.TextChannels.FirstOrDefault(c => c.Name.Contains(branchName))!.SendFilesAsync(filesToSend,
                    $"{branchName}: {description} ({Context.User.Username})| {DateTime.Now:MM_dd_yyyy} {DateTime.Now:hh:mm:ss}");
            }
            else
            {
                await Context.Channel.SendMessageAsync($"Ветка git-local/{branchName} не найдена");
            }

            foreach (var fileName in Directory.GetFiles("TempFiles"))
            {
                if (!fileName.Contains("InitFile"))
                    File.Delete(fileName);
            }
        }
    }
}