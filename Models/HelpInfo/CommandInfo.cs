﻿using System.Collections.Generic;
using System.Linq;

namespace DiscordBot_git.Models.HelpInfo
{
    public class CommandInfo
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Example { get; set; }
        public List<ArgumentInfo> Args { get; set; }

        public override string ToString()
        {
            var result = $"{Name}: {Description}\n";
            result += "ARGS:\n";
            return Args.Aggregate(result, (current, argument) => current + argument);
        }
    }
}