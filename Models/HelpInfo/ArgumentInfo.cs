﻿namespace DiscordBot_git.Models.HelpInfo
{
    public class ArgumentInfo
    {
        public string Name { get; set; }
        public string Description { get; set; }
        
        public override string ToString()
        {
            var result = $"{Name}: {Description}\n";
            return result;
        }
    }
}