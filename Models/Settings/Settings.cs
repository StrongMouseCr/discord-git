﻿using System.IO;
using Newtonsoft.Json.Linq;

namespace DiscordBot_git.Models.Settings
{
    public static class Settings
    {
        public static string Token { get; set; }

        public static void InitializeSettings()
        {
            var settings = JObject.Parse(File.ReadAllText("Settings.json"));
            Token = settings[nameof(Token)]!.ToString();
        }
    }
}