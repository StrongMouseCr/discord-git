﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using DiscordBot_git.Models.Settings;
using DiscordBot_git.Services;

namespace DiscordBot_git
{
    class Program
    {
        private static DiscordSocketClient _client;
        private static CommandService _commands;

        private CommandHandler _commandHandler;

        public static Task Main(string[] args) => new Program().MainAsync();
        
        private async Task MainAsync()
        {
            var client = new DiscordSocketClient();
            _client = client;
            
            _commands = new CommandService();
            _commandHandler = new CommandHandler(_client, _commands);
            await _commandHandler.InstallCommandsAsync();
            client.Log += LogAsync;
            Settings.InitializeSettings();

            await client.LoginAsync(TokenType.Bot, Settings.Token);
            await client.StartAsync();

            await Task.Delay(Timeout.Infinite);
        }

        private Task LogAsync(LogMessage log)
        {
            Console.WriteLine(log.ToString());
            return Task.CompletedTask;
        }
    }
}